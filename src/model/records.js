const mongoose = require( 'mongoose' );

const RecordSchema = new mongoose.Schema( {
  key    : { type : String, default : '' },
  counts : { type : Array, default : [] },
  value  : { type : String, default : '' },
}, {
  collection  : 'records',
  usePushEach : true,
  minimize    : false,
  versionKey  : false,
  autoIndex   : false,
  timestamps  : true,
} );

mongoose.model( 'Record', RecordSchema );

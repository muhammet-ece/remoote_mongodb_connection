/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const Table = require( 'cli-table' );
const fs = require( 'fs' );

const listRoute = ( name, router ) => {
  const table = new Table( { head : ['METHOD', 'PATH'] } );
  process.stdout.write( `\x1b[36m[ ------- ${( name || 'HOME' ).toUpperCase()} ------- ]\x1b[89m\n` );
  router.stack.forEach( ( r ) => {
    if ( r.route && r.route.path ) {
      table.push( [r.route.stack[0].method.toUpperCase(), `${name ? '/' : ''}${name.toLowerCase()}${r.route.path}`] );
    }
  } );
  process.stdout.write( table.toString() );
  process.stdout.write( '\n\n' );
  return table;
};
module.exports = ( router ) => {
  const routes = fs.readdirSync( `${__dirname}/../route` );
  for ( let i = 0; i < routes.length; i += 1 ) {
    const name = routes[i].replace( '.js', '' );
    const r = require( `${__dirname}/../route/${name}` )();

    const path = name !== 'home' ? name : '';
    router.use( `/${path}`, r );
    listRoute( path, r );
  }
};
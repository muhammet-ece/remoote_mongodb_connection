/* eslint-disable no-param-reassign */
const express = require( 'express' );
const helmet = require( 'helmet' );
const compression = require( 'compression' );
const bodyParser = require( 'body-parser' );
const expressSession =  require( 'express-session' );
const { JSONResponse } = require( './api-response' );

module.exports = () => {
  const app = express();
  const router = express.Router();

  /**
  * Helmet helps you secure your Express apps by setting various HTTP headers.
  */
  app.use( helmet() );
  app.use( helmet.xssFilter() );
  app.use( helmet.frameguard() );

  /**
  * Node.js compression middleware.
  */
  app.use( compression() );

  /**
  * Node.js body parsing middleware.
  * JSON
  * URL Encode
  */
  app.use( bodyParser.json( {
    limit : '500mb',
  } ) );
  app.use( bodyParser.urlencoded( {
    extended : true,
  } ) );
  app.use( expressSession( {
    secret            : '6jNVPXhx4bbJ',
    resave            : false,
    saveUninitialized : true,
    secure            : true,
  } ) );
  app.use( router );
  
  app.listen( process.env.PORT );
  console.log( `Server Listenning on ${$config.http.port} Port!` );

  return router;
};

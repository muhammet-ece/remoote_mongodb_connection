/* eslint-disable max-classes-per-file */
class ApiError {
  constructor (
    code = 1,
    msg = 'Service Unavailable.',
    records = null,
  ) {
    this.code = code;
    this.msg = msg;
    this.records = records;
  }

  toJSON () {
    return {
      code    : this.code,
      msg     : this.msg,
      records : this.records,
    };
  }
}

class JSONResponse {
  constructor (
    records = null,
    code = 0,
    msg = 'Successful',
  ) {
    this.code = code;
    this.msg = msg;
    this.records = records;
  }

  toJSON () {
    return {
      code    : this.code,
      msg     : this.msg,
      records : this.records,
    };
  }
}

module.exports = {
  ApiError,
  JSONResponse,
};

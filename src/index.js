const config = require( './lib/config' );

config();

require( './model/records' );
const server = require( './lib/server' );
const mongodb = require( './lib/mongodb' );
const loadRoute = require( './lib/load-router' );


( async () => {
  await mongodb();
  const router = server();
  loadRoute( router );
} )();

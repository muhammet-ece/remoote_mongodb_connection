const mongoose = require( 'mongoose' );
const { JSONResponse, ApiError } = require( '../lib/api-response' );

const Record = mongoose.model( 'Record' );
const { ObjectId } = mongoose.Types;

const createRecord = async ( req, res, next ) => {
  try {

  } catch ( error ) {

  }
};
const getRecord = async ( req, res, next ) => {
  try {

  } catch ( error ) {

  }
};
const listRecords = async ( req, res ) => {
  try {
    const {
      startDate, endDate, minCount, maxCount,
    } = req.body;
    const start_date = new Date( startDate ).toISOString();
    const end_date = new Date( endDate ).toISOString();
    const records = await Record.aggregate( [
      {
        $project : {
          countTotal : { $sum : '$counts' },
          createdAt  : 1,
          key        : 1,
        },
      },
      {
        $match : {
          $and : [
            { countTotal : { $gte : minCount, $lte : maxCount } },
            { createdAt : { $gte : new Date( start_date.toString() ), $lte : new Date( end_date.toString() ) } },
          ],
        },
      },
    ] );
    return res.json( new JSONResponse( records ) );
  } catch ( error ) {
    return res.json( new ApiError( error ) );
  }
};
module.exports = {
  createRecord,
  getRecord,
  listRecords,
};

const express = require( 'express' );
const { createRecord, getRecord, listRecords } = require( '../service/record' );

const router = express.Router();

module.exports = () => {
  /*  <-------------- API --------------> */
  router.post( '/create', createRecord );
  router.post( '/get', getRecord );
  router.post( '/list', listRecords);

  return router;
};
